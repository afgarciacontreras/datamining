The files in the "python" folder are used to process the Yelp dataset, in this order:

1. preprocessing.py
2. category_signal.py
3. word_signal.py
4. naivebayes-prob.py
5. GrangerCaus.py
6. naivebayes-class.py

Directories are hard-coded in all the files, mostly due to lack of time to create proper functions.

The following variables need to be changed:
*	preprocessing.py
	-	folder: 		the location of the Yelp dataset
	- 	out_folder:		the location in which all the results will be stored
*	category_signal.py, category_signal.py, naivebayes-prob.py, GrangerCaus.py, naivebayes-class.py
	- 	data_folder: 	the location in which all the results are stored
* 	word_signal.py
	- 	pos_words: 		list of top positive words (must be manually selected from reviews_accum-pos_words.csv file)
	- 	neg_words: 		list of top negative words (must be manually selected from reviews_accum-neg_words.csv file)
* 	naivebayes-prob.py
	-	big_words:		list of top words (must be manually selected from reviews_accum-pos_words.csv and reviews_accum-neg_words.csv files)
* 	naivebayes-class.py
	- 	selected_words:	list of words selected by analyzing the results of the Granger causality tests
	
There is also additional manual work to do between preprocessing.py and category_signal.py: simply take the reviews.csv file and create a filter that shows all the reviews from a single business. Use these reviews to create a new file, reviews_x.csv, which is required by all the subsequent steps.

I apologize if this sounds complicated, but I have run out of time. If you want to try things out: Good luck!


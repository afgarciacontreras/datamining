# -*- coding: utf-8 -*-
"""
Created on Tue May 12 11:28:11 2015

@author: Angel
"""
from collections import OrderedDict
import csv
import numpy as np
import statsmodels.tsa.stattools as stattools


def printGrangerResults(grangerResults):
    strRes = ''
    for i in range(1,5):
        lagResults = grangerResults[i][0]
        strRes = strRes + '\n'
        strRes = strRes + 'number of lags (no zero): {0}\n'.format(i)
        
        ssr_F = lagResults['ssr_ftest']
        strRes = strRes + 'ssr based F test:         F=%-8.4f, p=%-8.4f, df_denom=%d, df_num=%d \n' \
                % (ssr_F[0],ssr_F[1],ssr_F[2],ssr_F[3])
        
        ssr_chi2 = lagResults['ssr_chi2test']
        strRes = strRes + 'ssr based chi2 test:      chi2=%-8.4f, p=%-8.4f, df=%d \n' \
                % (ssr_chi2[0],ssr_chi2[1],ssr_chi2[2])
        
        llr_chi2 = lagResults['lrtest']
        strRes = strRes + 'likelihood ratio test:    chi2=%-8.4f, p=%-8.4f, df=%d \n' \
                % (llr_chi2[0],llr_chi2[1],llr_chi2[2])
        
        params_ftest = lagResults['params_ftest']
        strRes = strRes + 'parameter F test:         F=%-8.4f, p=%-8.4f, df_denom=%d, df_num=%d \n' \
                % (params_ftest[0],params_ftest[1], \
                params_ftest[2],params_ftest[3])
        
    return strRes


data_folder          = \
r"C:\Users\Angel\Documents\DATAMINING\git\datasets3\\"

fname = data_folder + "reviews_accum.csv"
info   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        trim   = int(review['trimester'])
        if year not in info:
            info[year] = OrderedDict()
        info[year][trim] = review

total_pos  = []
total_neg = []
for year in info:    
    for trimester in info[year].keys():
        review       = info[year][trimester]
        total_pos.append(int(review['pos_count']))
        total_neg.append(int(review['neg_count']))
        
fname      = data_folder + "reviews_pos_wc_signal.csv"
info_pos   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        trim   = int(review['trimester'])
        if year not in info_pos:
            info_pos[year] = OrderedDict()
        info_pos[year][trim] = review

pos_w_cnt = OrderedDict()
pos_wrds = None
for year in info_pos:    
    for trimester in info_pos[year].keys():
        review       = info_pos[year][trimester]
        if pos_wrds is None:
            pos_wrds    = [ w for w in review.keys() if w!='year' and w!='trimester' ]
        for word in pos_wrds:
            if word not in pos_w_cnt.keys():
                pos_w_cnt[word] = []
            pos_w_cnt[word].append(int(review[word]))

fname      = data_folder + "granger_data_pos.txt"
with open(fname,'w') as rev_file:
    rev_file.write('======================\n')
    rev_file.write('||  POSITIVE WORDS  ||\n')
    rev_file.write('======================\n\n')
    for word in pos_wrds:
        rev_file.write('GRANGER CAUSALIITY ANALYSIS FOR:\n')
        rev_file.write('{0}\n'.format(word))
        pos_w_signal = pos_w_cnt[word]
        dataSet = np.concatenate(([total_pos],[pos_w_signal]),axis=0)
        dataSet = np.transpose(dataSet)
        grangerResults = stattools.grangercausalitytests(dataSet,4,verbose=False)
        rev_file.write(printGrangerResults(grangerResults))
        rev_file.write('\n')
        rev_file.flush()

fname      = data_folder + "reviews_neg_wc_signal.csv"
info_neg   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        trim   = int(review['trimester'])
        if year not in info_neg:
            info_neg[year] = OrderedDict()
        info_neg[year][trim] = review

neg_w_cnt = OrderedDict()
neg_wrds = None
for year in info_neg:    
    for trimester in info_neg[year].keys():
        review       = info_neg[year][trimester]
        if neg_wrds is None:
            neg_wrds    = [ w for w in review.keys() if w!='year' and w!='trimester' ]
        for word in neg_wrds:
            if word not in neg_w_cnt.keys():
                neg_w_cnt[word] = []
            neg_w_cnt[word].append(int(review[word]))

fname      = data_folder + "granger_data_neg.txt"
with open(fname,'w') as rev_file:
    rev_file.write('======================\n')
    rev_file.write('||  NEGATIVE WORDS  ||\n')
    rev_file.write('======================\n\n')
    for word in neg_wrds:
        rev_file.write('GRANGER CAUSALIITY ANALYSIS FOR:\n')
        rev_file.write('{0}\n'.format(word))
        neg_w_signal = neg_w_cnt[word]
        dataSet = np.concatenate(([total_neg],[neg_w_signal]),axis=0)
        dataSet = np.transpose(dataSet)
        grangerResults = stattools.grangercausalitytests(dataSet,4,verbose=False)
        rev_file.write(printGrangerResults(grangerResults))
        rev_file.write('\n')
        rev_file.flush()
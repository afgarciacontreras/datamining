# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 23:15:09 2015

@author: Angel
"""
from sklearn.feature_extraction.text import CountVectorizer
from collections import OrderedDict
import numpy as np
import csv

data_folder          = \
r"C:\Users\Angel\Documents\DATAMINING\git\datasets2\\"
fname = data_folder + "reviews_x.csv"
train_data  = []
info   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        month  = int(review['month'])
        if year not in info:
            info[year] = OrderedDict()
        if month not in info[year]:
            info[year][month] = list()
        info[year][month].append(review)


total_info = []
corpus_pos = []
corpus_neg = []
classif_data = []
count_pos = 0
count_neg = 0
for year in info:
    months = [(1,2,3),(4,5,6),(7,8,9),(10,11,12)]
    for trimester in months:
        trim_id         = trimester[2]/3
        trim_corpus     = []
        count_pos       = 0
        count_neg       = 0        
        for month in trimester:
            if month in info[year].keys():
                for review in info[year][month]:
                    classif = 'pos' if str(review['goodness']) == 'good' else 'neg'
                    if classif == 'pos':
                        count_pos += 1
                        text    = review['text']
                        corpus_pos.append(text)
                    else:
                        count_neg +=1
                        text    = review['text']
                        corpus_neg.append(text)
                    trim_corpus.append(text)
        vectorizer = CountVectorizer(stop_words='english', \
                ngram_range=(1,2),token_pattern=r'\b\w+\b',min_df=1)
        word_count_trim = OrderedDict()
        if len(trim_corpus) > 0:
            X = vectorizer.fit_transform(trim_corpus)
            features = vectorizer.get_feature_names()
            term_count = np.sum(X.toarray(),0)
            for i in range(len(features)):
                word = features[i]
                count = term_count[i]
                word_count_trim[word] = count            
        line = ['{0}-{1}'.format(year,trim_id),year,trim_id,count_pos,count_neg, \
            str(word_count_trim)]
        total_info.append(line)
        
fname = data_folder + "reviews_accum.csv"
with open(fname,'w') as rev_file:
    csv_writer = csv.writer(rev_file)
    for line in total_info:
        csv_writer.writerow(line)
        
word_count_pos = OrderedDict()
if len(corpus_pos) > 0:
    vectorizer = CountVectorizer(stop_words='english', \
            ngram_range=(1,2),token_pattern=r'\b\w+\b',min_df=1)
    X = vectorizer.fit_transform(corpus_pos)
    features = vectorizer.get_feature_names()
    term_count = np.sum(X.toarray(),0)
    for i in range(len(features)):
        word = features[i]
        count = term_count[i]
        word_count_pos[word] = count
   
word_count_neg = OrderedDict()
if len(corpus_neg) > 0:
    vectorizer = CountVectorizer(stop_words='english', \
            ngram_range=(1,2),token_pattern=r'\b\w+\b',min_df=1)
    X = vectorizer.fit_transform(corpus_neg)
    features = vectorizer.get_feature_names()
    term_count = np.sum(X.toarray(),0)
    for i in range(len(features)):
        word = features[i]
        count = term_count[i]
        word_count_neg[word] = count

wc_pos_ordered = OrderedDict(sorted(word_count_pos.items(), key=lambda t:t[1],reverse=True))
wc_neg_ordered = OrderedDict(sorted(word_count_neg.items(), key=lambda t:t[1],reverse=True))

fname = data_folder + "reviews_accum-pos_words.csv"
with open(fname,'w') as rev_file:
    csv_writer = csv.writer(rev_file)
    for key in wc_pos_ordered:
        row = [key,wc_pos_ordered[key]]
        csv_writer.writerow(row)
fname = data_folder + "reviews_accum-neg_words.csv"
with open(fname,'w') as rev_file:
    csv_writer = csv.writer(rev_file)
    for key in wc_neg_ordered:
        row = [key,wc_neg_ordered[key]]
        csv_writer.writerow(row)
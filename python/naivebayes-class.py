# -*- coding: utf-8 -*-
"""

@author: Angel
"""

# Naive-Bayes classifier for probabilities

from collections import OrderedDict
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
import csv
import numpy as np


#dataset2
#selected_words = ['food','lamb','jazz','service','place','nice','wine','live']

#dataset3
selected_words = ['good','just','great','church','pizza']

data_folder          = \
r"C:\Users\Angel\Documents\DATAMINING\git\datasets3\\"

fname = data_folder + "reviews_x.csv"
train_data  = []
info   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        month  = int(review['month'])
        if year not in info:
            info[year] = OrderedDict()
        if month not in info[year]:
            info[year][month] = list()
        info[year][month].append(review)

years_list = range(2010,2014)

for base_y in years_list:    
    years = [base_y,base_y+1]

    total_info  = []
    classif_data = []
    count_pos = 0
    count_neg = 0
    
    data_text   = []
    target      = []
    test        = []
    test_text   = []
    for year in info:
        months = [(1,2,3),(4,5,6),(7,8,9),(10,11,12)]
        for trimester in months:
            for month in trimester:
                if month in info[year].keys():
                    for review in info[year][month]:
                        classif = 'pos' if str(review['goodness']) == 'good' else 'neg'
                        text    = review['text']
                        if classif == 'pos':
                            classif = 1
                        else:
                            classif = 0
                        
                        if year==years[0]:
                            data_text.append(text)
                            target.append(classif)
                        if year==years[1]:
                            test_text.append(text)
                            test.append(classif)
    
    vectorizer = CountVectorizer(stop_words='english', \
                ngram_range=(1,2),token_pattern=r'\b\w+\b',min_df=1, \
                vocabulary = selected_words)
    X = vectorizer.fit_transform(data_text)
    features = vectorizer.get_feature_names()
    
    gnb = MultinomialNB()
    #gnb = GaussianNB()
    model = gnb.fit(X,target)
    
    probabilities = np.exp(np.array(model.feature_log_prob_))
    
    vectorizer = CountVectorizer(stop_words='english', \
                ngram_range=(1,2),token_pattern=r'\b\w+\b',min_df=1, \
                vocabulary = selected_words)
    
    Y = vectorizer.fit_transform(test_text)
    
    prediction = gnb.predict(Y)
    
    tp = 0
    tn = 0
    count = 0
    
    for i in range(len(test)):
        for j in range(1,len(prediction)):
            if prediction[i] == prediction[j] and test[i] == test[j]:
                tp += 1
            elif prediction[i]!=prediction[j] and test[i]!=test[j]:
                tn = tn+1
            count = count+1
    rand_index  = (tp + tn)/float(count);
    
    fname = data_folder + "NBclass-{0}-{1}.txt".format(years[0],years[1])
    with open(fname,'w') as rev_file:
        rev_file.write("Classes: {0}\n".format(str(gnb.classes_)))
        rev_file.write("Probabilities:\n")
        rev_file.write("{0}\n".format(str(features)))
        for row in probabilities:
            rev_file.write("{0}\n".format(str(row)))
        rev_file.write("{1} Train Data: {0}\n".format(str(target),years[0]))
        rev_file.write("{1} Prediction: {0}\n".format(str(prediction),years[1]))
        rev_file.write("{1} Test Data:  {0}\n".format(str(test),years[1]))
        rev_file.write("RAND INDEX:  {0}\n".format(str(rand_index)))
        print 'DONE'
# -*- coding: utf-8 -*-
"""
Created on Wed May 06 09:43:31 2015

@author: Angel
"""

# Naive-Bayes classifier for probabilities

from collections import OrderedDict
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
import csv
import numpy as np

#dataset2
big_words = ['good','great','montreal','food','lamb','jazz','service','modavie','place','nice','wine','live','like','music','just','meal']


#dataset3
#big_words = ['church','place','good','food','great','pizza','just','like','brew','pittsburgh','really','beers','beer','time']

data_folder          = \
r"C:\Users\Angel\Documents\DATAMINING\git\datasets2\\"

fname = data_folder + "reviews_x.csv"
train_data  = []
info   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        month  = int(review['month'])
        if year not in info:
            info[year] = OrderedDict()
        if month not in info[year]:
            info[year][month] = list()
        info[year][month].append(review)


total_info = []
classif_data = []
count_pos = 0
count_neg = 0

data_text = []
target = []
for year in info:
    months = [(1,2,3),(4,5,6),(7,8,9),(10,11,12)]
    for trimester in months:
        for month in trimester:
            if month in info[year].keys():
                for review in info[year][month]:
                    classif = 'pos' if str(review['goodness']) == 'good' else 'neg'
                    text    = review['text']
                    data_text.append(text)
                    if classif == 'pos':
                        target.append(1)
                    else:
                        target.append(-1)

vectorizer = CountVectorizer(stop_words='english', \
            ngram_range=(1,2),token_pattern=r'\b\w+\b',min_df=1, \
            vocabulary = big_words)
X = vectorizer.fit_transform(data_text)
features = vectorizer.get_feature_names()

gnb = MultinomialNB()
#gnb = GaussianNB()
model = gnb.fit(X,target)

probabilities = np.exp(np.array(model.feature_log_prob_))

fname = data_folder + "NBprobs.csv"
with open(fname,'w') as rev_file:
    csv_writer = csv.writer(rev_file)
    for row in probabilities:
        csv_writer.writerow(row)
    print 'DONE'
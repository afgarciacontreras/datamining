# -*- coding: utf-8 -*-
"""
Created on Thu Apr 02 11:23:12 2015

@author: Angel
"""

import json
import dateutil.parser as dateparser
import csv
import chardet
from langdetect import detect
import codecs

decoder             = json.JSONDecoder()
folder              = \
r"C:\Users\Angel\Documents\DATAMINING\yelp_dataset_challenge_academic_dataset"
out_folder          = \
r"C:\Users\Angel\Documents\DATAMINING\git\datasets3\\"
business_filename   = "\\yelp_academic_dataset_business.json"
business_file       = open(folder+business_filename,"r+")
business_list       = []
business_id_list    = []
for line in business_file:
    business = decoder.decode(line)
    if business['city'] == 'Pittsburgh':
        business_list.append(business)
        business_id_list.append(business['business_id'])

business_file.close()
reviews_filename    = "\\yelp_academic_dataset_review.json"
reviews_file        = open(folder+reviews_filename,"r+")
reviews_list        = []
reviewers_list      = []
for line in reviews_file:
    review = decoder.decode(line)
    if review['business_id'] in business_id_list:
        reviewText = review['text']
        #encoding = chardet.detect(reviewText)['encoding']
        try:
            lang = detect(reviewText)
            if lang == 'fr':
                continue
        except Exception as e:
            continue
        reviews_list.append(review)
        if review['user_id'] not in reviewers_list:
            reviewers_list.append(review['user_id'])
reviews_file.close()
#users_filename      = "\\yelp_academic_dataset_user.json"
#users_file          = open(folder+users_filename,"r+")
#users_list          = []
#for line in users_file:
#    user = decoder.decode(line)
#    if user['user_id'] in reviewers_list:
#        users_list.append(user)
#users_file.close()

business_csv_fname  = "business.csv"
with codecs.open(out_folder+business_csv_fname,"w",encoding='ascii',errors='ignore') as csv_file:
    csv_writer = csv.writer(csv_file)
    for business in business_list:
        address = business['full_address'].replace("\n"," ")
        address = address.encode('ascii', 'ignore')
        line    = [business['business_id'],\
                business['name'],\
                address,\
                business['city'],\
                business['state'],\
                business['latitude'],\
                business['longitude'],\
                business['stars'],\
                business['review_count'],\
                business['open']]
        line    = [str(s).encode('ascii','ignore') if isinstance(s,str) \
                else s for s in line]
        try:
            csv_writer.writerow(line)
        except UnicodeEncodeError as err:
            print line
    csv_file.close()

review_csv_fname  = "review.csv"
with open(out_folder+review_csv_fname,"w") as csv_file:
    csv_writer = csv.writer(csv_file)
    for review in reviews_list:
        reviewText = review['text']
        #encoding = chardet.detect(reviewText)['encoding']
        asciiText   = reviewText.encode('ascii', 'ignore')
        asciiText   = asciiText.replace("\n"," ")
        date        = review['date']
        dateObj     = dateparser.parse(date)
        month       = str(dateObj.month)
        year        = str(dateObj.year)
        stars       = int(review['stars'])
        goodness    = 'nogood' if stars <= 3 else 'good'
        line        = [review['business_id'],\
                    review['user_id'],\
                    review['stars'],\
                    year, \
                    month,\
                    review['votes']['funny'],\
                    review['votes']['useful'],\
                    review['votes']['cool'],\
                    goodness,\
                    asciiText]
        csv_writer.writerow(line)
    csv_file.close()
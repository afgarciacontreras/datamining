# -*- coding: utf-8 -*-
"""
Created on Sun May 03 20:59:35 2015

@author: Angel
"""
from sklearn.feature_extraction.text import CountVectorizer
from collections import OrderedDict
import numpy as np
import csv

data_folder          = \
r"C:\Users\Angel\Documents\DATAMINING\git\datasets3\\"
fname = data_folder + "reviews_x.csv"

#dataset2
#pos_words = ['good','great','montreal','food','lamb','jazz','service','modavie','place','nice','wine','live']
#neg_words = ['lamb','good','like','food','service','great','live','jazz','music','just','meal','nice']

#dataset3
pos_words = ['church','place','good','food','great','pizza','just','like','brew','pittsburgh','really','beers']
neg_words = ['beer','food','church','good','place','just','like','pizza','brew','beers','time','great']

fname = data_folder + "reviews_x.csv"
train_data  = []
info   = OrderedDict()
with open(fname,'r') as rev_file:    
    csv_reader  = csv.DictReader(rev_file)
    for review in csv_reader:
        year   = int(review['year'])
        month  = int(review['month'])
        if year not in info:
            info[year] = OrderedDict()
        if month not in info[year]:
            info[year][month] = list()
        info[year][month].append(review)

pos_reviews = []
neg_reviews = []

for year in info:
    months = [(1,2,3),(4,5,6),(7,8,9),(10,11,12)]
    for trimester in months:
        review_pos = OrderedDict()
        review_neg = OrderedDict()
        pos_counts = OrderedDict()
        for pos_w in pos_words:
            pos_counts[pos_w] = 0
        neg_counts = OrderedDict()
        for neg_w in neg_words:
            neg_counts[neg_w] = 0
            
        for month in trimester:
            if month in info[year].keys():
                for review in info[year][month]:
                    classif = 'pos' if str(review['goodness']) == 'good' else 'neg'
                    if classif == 'pos':
                        text    = review['text'].lower()
                        for word in pos_words:
                            if word in text:
                                pos_counts[word] += 1
                    else:
                        text    = review['text'].lower()
                        for word in neg_words:
                            if word in text:
                                neg_counts[word] += 1
        review_pos['year'] = year
        review_pos['trimester'] = trimester[2]/3
        for word in pos_counts:
            review_pos[word] = pos_counts[word]
        pos_reviews.append(review_pos)
        
        review_neg['year'] = year
        review_neg['trimester'] = trimester[2]/3
        for word in neg_counts:
            review_neg[word] = neg_counts[word]
        neg_reviews.append(review_neg)

fname = data_folder + "reviews_pos_wc_signal.csv"
with open(fname,'w') as rev_file:    
    header = ['year','trimester'] + pos_words
    csv_writer  = csv.DictWriter(rev_file,header)
    csv_writer.writeheader()
    csv_writer.writerows(pos_reviews)

fname = data_folder + "reviews_neg_wc_signal.csv"
with open(fname,'w') as rev_file:    
    header = ['year','trimester'] + neg_words
    csv_writer  = csv.DictWriter(rev_file,header)
    csv_writer.writeheader()
    csv_writer.writerows(neg_reviews)